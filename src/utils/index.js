const bcrypt = require('bcryptjs')

const cryptPass = (pass)=>{
    const salt = bcrypt.genSaltSync();
    return bcrypt.hashSync(pass, 10)
    
}

const isPassword = async (password, encodedPassword) => {                    
    const retorno = await bcrypt.compare(password, encodedPassword)
    console.log('Retorno',retorno)
    return retorno
}


module.exports = { cryptPass, isPassword}