

module.exports = (sequelize, DataType)=>{
    const Produtos = sequelize.define('Produtos', {
        id: {
            type: DataType.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        descricao: {
            type: DataType.STRING,
            allowNull: false,
            validate: {
                notEmpty: true,
            },
        },
        imagem: {
            type: DataType.STRING,
            allowNull: false,
            unique: {
                msg: 'imagem erro'
            },
            validate: {
                notEmpty: true,
                
            },
        },
       
    },{
        indexes:[
        {
            unique: true,
            fields: ['descricao']
        }],
        hooks: {
            beforeCreate: prod => {
                
               
                
            },
            beforeUpdate: prod => {
                
            }
        }
                
        })
    
   
    return Produtos
}