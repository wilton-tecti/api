const {cryptPass, isPassword} = require('../utils')

module.exports = (sequelize, DataType)=>{
    const Users = sequelize.define('Users', {
        id: {
            type: DataType.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        name: {
            type: DataType.STRING,
            allowNull: false,
            validate: {
                notEmpty: true,
            },
        },
        email: {
            type: DataType.STRING,
            allowNull: false,
            unique: {
                msg: 'Email em uso'
            },
            validate: {
                notEmpty: true,
                isEmail: true,
            },
        },
        password: {
            type: DataType.STRING,
            allowNull: false,
            validate: {
                notEmpty: true,
            },
        },
    },{
        indexes:[
        {
            unique: true,
            fields: ['email']
        }],
        hooks: {
            beforeCreate: user => {
                
                user.set('password',cryptPass(user.password) );
                
            },
            beforeUpdate: user => {
                user.set('password',cryptPass(user.password) );
            }
        }
                
        })
    
    Users.isPassword = (password, encodedPassword) =>{
        return isPassword(password, encodedPassword)
    }
    return Users
}