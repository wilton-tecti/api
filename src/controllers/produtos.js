const HttpStatus = require('http-status');

const defaultResponse = (data, statusCode = HttpStatus.OK) => ({
  data,
  statusCode,
});

const errorResponse = (message, statusCode = HttpStatus.BAD_REQUEST) => defaultResponse({
  error: message,
}, statusCode);

class ProdutosController {
  constructor(Produtos) {
    this.Produtos = Produtos;
  }

  getAll() {
    return this.Produtos.findAll({})
    .then(result => defaultResponse(result))
    .catch(error => errorResponse(error.message));
  }

  getById(params) {
    return this.Produtos.findOne({
      where: params,
    })
    .then(result => defaultResponse(result))
    .catch(error => errorResponse(error.message));
  }

  create(data) {
    return this.Produtos.create(data)
    .then(result => defaultResponse(result, HttpStatus.CREATED))
    .catch(error => errorResponse(error.message, HttpStatus.UNPROCESSABLE_ENTITY));
  }

  update(data, params) {
    return this.Produtos.update(data, {
      where: params,
    })
    .then(result => defaultResponse(result))
    .catch(error => errorResponse(error.message, HttpStatus.UNPROCESSABLE_ENTITY));
  }

  delete(params) {
    return this.Produtos.destroy({
      where: params,
    })
    .then(result => defaultResponse(result, HttpStatus.NO_CONTENT))
    .catch(error => errorResponse(error.message, HttpStatus.UNPROCESSABLE_ENTITY));
  }
}

module.exports = ProdutosController;