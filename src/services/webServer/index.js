const express = require('express');
const bodyParser = require('body-parser');
let app = express();
const router = require('../../router')
const auth = require('../../midleware/auth')
console.debug('AUTH', auth())
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())
app.use(auth().initialize())
app.use(express.static('../../../public'))

app.use('/api',router)

const initialize = () =>{  
   
    

    console.log("Initialize webServer");
    
    app.listen(3000, function () {
        console.log("App runing on port 3000");
    });

}


module.exports = {
    initialize
}