const Sequelize = require('sequelize');
const fs = require('fs');
const path = require('path');


let sequelize = new Sequelize('database','user','password', {
  host: 'localhost',
  dialect: 'sqlite',
  operatorsAliases: false,    
  // SQLite only
  storage: './db.sqlite'
});

const loadModels = (sequelize) =>{
  const dir = path.join(__dirname, '../../models');
  const models = [];
  fs.readdirSync(dir).forEach(file => {
    const modelDir = path.join(dir, file);
    const model = sequelize.import(modelDir);
    models[model.name] = model;
  });
  return models;

} 

  

let database = {
    sequelize,
    Sequelize,
    models: {},
  }
    
database.models =  loadModels(sequelize);
    


module.exports = database

