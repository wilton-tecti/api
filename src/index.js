const webServer = require('./services/webServer')
const WebDatabase = require('./services/WebDatabase')

async function startup(){
  
  console.log("start server")

  console.log("start database")
  await WebDatabase.sequelize.sync()
  //console.debug(con)

  console.log("initalize web serve module")
  await webServer.initialize()

  console.log("server running")

 }


 startup()
