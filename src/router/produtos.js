const express = require('express');
const router = express.Router()

const upload = require('../midleware/storage')
const auth = require('../midleware/auth')
let WebDatabase = require('../services/WebDatabase')  
const ProdutosController = require('../controllers/produtos');

let Produtos = WebDatabase.models.Produtos
const Controller = new ProdutosController(Produtos);
  

router.get('/', async (req, res) =>{
  
  const response = await Controller.getAll();     
  res.status(response.statusCode);
  res.json(response.data);         
  
  
})

router.post('/', upload.single('imagem'), async (req, res) => {
  
  const response = await Controller.create(req.body);       
  res.status(response.statusCode);
  res.json(response.data);
  
});

router.get('/:id', async (req, res) => {
  
  const response = await Controller.getById(req.params)
  res.status(response.statusCode);
  res.json(response.data);  

})

router.put('/:id', async (req, res) => {
  
  const response = await Controller.update(req.body, req.params)
  res.status(response.statusCode);
  res.json(response.data);
  
})

router.delete('/:id', async (req, res) => {

  const response = await Controller.delete(req.params)
  res.status(response.statusCode);
  res.json(response.data);
})
  
module.exports = router