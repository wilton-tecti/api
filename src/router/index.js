const express = require('express');
const router = express.Router()

const usersRouter = require('./users')
const authRouter = require('./auth')
const produtosRouter = require('./produtos')

router.use('/users', usersRouter)
router.use('/auth', authRouter)
router.use('/produtos', produtosRouter)

module.exports = router