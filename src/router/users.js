const express = require('express');
const router = express.Router()

let WebDatabase = require('../services/WebDatabase')  
let Users = WebDatabase.models.Users

const auth = require('../midleware/auth')
const UsersController = require('../controllers/users');
const usersController = new UsersController(  Users);
  

router.get('/',auth().authenticate(), async (req, res) =>{
  
  const response = await usersController.getAll();     
  res.status(response.statusCode);
  res.json(response.data);         
  
  
})

router.post('/', async (req, res) => {
  
  const response = await usersController.create(req.body);       
  res.status(response.statusCode);
  res.json(response.data);
  
});

router.get('/:id', async (req, res) => {
  
  const response = await usersController.getById(req.params)
  res.status(response.statusCode);
  res.json(response.data);  

})

router.put('/:id', async (req, res) => {
  
  const response = await usersController.update(req.body, req.params)
  res.status(response.statusCode);
  res.json(response.data);
  
})

router.delete('/:id', async (req, res) => {

  const response = await usersController.delete(req.params)
  res.status(response.statusCode);
  res.json(response.data);
})
  
module.exports = router