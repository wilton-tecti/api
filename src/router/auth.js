const  HttpStatus = require('http-status');
const jwt = require('jwt-simple');
const express = require('express')
const cryptPass = require('../utils')
const Users = require('../services/WebDatabase').models.Users
const router = express.Router()
const config = require('../config')

 module.exports =  router.post('/', async (req, res) => {
    if (req.body.email && req.body.password) {
      
      const email = req.body.email;
      const passwordText = req.body.password;
      
      try{
      const user = await Users.findOne({ where: { email } })
      
        if ( await Users.isPassword( passwordText, user.password)) {
          const payload = { id: user.id };
          
          res.json({
            token: jwt.encode(payload, config.jwtSecret),
          });
        } else {
          console.log("USUARIO OU SENHA NAO ENCONTRADO")
          res.sendStatus(HttpStatus.UNAUTHORIZED);
        }
      
      }catch(error){
        console.log("OCORREU UM ERRO", error)
        res.sendStatus(HttpStatus.UNAUTHORIZED)
      }
    } else {
      res.sendStatus(HttpStatus.UNAUTHORIZED);
    }
  });

